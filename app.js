const http=require('http');
const express=require('express');
const app=express();
const bodyParser=require('body-parser');
app.use(bodyParser.urlencoded({extended:false}));
app.use('/hello',(req, res, next)=>{
    //console.log('Inside Middleware!!')
    //next();
    res.send('<form action="/test" method="POST"><input type="test" name="title"/><button type="submit">Click</button></form>');
});
app.use('/test',(req, res, next)=>{
    console.log(req.body)
    //next();
    res.send('<h1>Test</h1>');
});
app.use('/',(req, res, next)=>{
    console.log('Root Middleware2!!')
    //next();
    //res.send('<h1>Welcome</h1>');
});

// const server=http.createServer(app);
// server.listen(3000);
app.listen(3000);